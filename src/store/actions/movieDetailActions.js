export const ACTION_MOVIE_DETAIL_FETCH_BY_ID = 'movie-detail:FETCHING_BY_ID'
export const ACTION_MOVIE_DETAIL_SET_BY_ID = 'movie-detail:SET_BY_ID'
export const ACTION_MOVIE_DETAIL_ERROR = 'movie-detail:ERROR'

export const movieDetailFetchingByIdAction = payload => ({
    type: ACTION_MOVIE_DETAIL_FETCH_BY_ID,
    payload
})

export const movieDetailSetByIdAction = payload => ({
    type: ACTION_MOVIE_DETAIL_SET_BY_ID,
    payload
})

export const movieDetailErrorAction = payload => ({
    type: ACTION_MOVIE_DETAIL_ERROR,
    payload
})


export const ACTION_PROFILE_CHECK_EXISTING = 'profile:CHECK_EXISTING'
export const ACTION_PROFILE_FETCH = 'profile:FETCH'
export const ACTION_PROFILE_SET = 'profile:SET'
export const ACTION_PROFILE_LOGOUT = 'profile:LOGOUT'
export const ACTION_PROFILE_ERROR = 'profile:ERROR'

export const profileCheckExistingAction = () => ({
    type: ACTION_PROFILE_CHECK_EXISTING
})

export const profileErrorAction = payload => ({
    type: ACTION_PROFILE_ERROR,
    payload
})

export const profileFetchAction = payload => ({
    type: ACTION_PROFILE_FETCH,
    payload
})

export const profileSetAction = payload => ({
    type: ACTION_PROFILE_SET,
    payload
})

export const profileLogoutAction = () => ({
    type: ACTION_PROFILE_LOGOUT
})
export const ACTION_FAVOURITES_FETCH = 'favourites:FETCH'
export const ACTION_FAVOURITES_SET = 'favourites:SET'
export const ACTION_FAVOURITES_ADD = 'favourites:ADD'
export const ACTION_FAVOURITES_ERROR = 'favourites:ERROR'

export const favouritesFetchAction = payload => ({
    type: ACTION_FAVOURITES_FETCH,
    payload
})

export const favouritesAddAction = payload => ({
    type: ACTION_FAVOURITES_ADD,
    payload
})

export const favouritesSetAction = payload => ({
    type: ACTION_FAVOURITES_SET,
    payload
})

export const favouritesErrorAction = payload => ({
    type: ACTION_FAVOURITES_ERROR,
    payload
})

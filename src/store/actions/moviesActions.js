export const ACTION_MOVIES_SET = 'movies:SET'
export const ACTION_MOVIES_FETCHING = 'movies:FETCHING'
export const ACTION_MOVIES_ERROR = 'movies:ERROR'



export const moviesFetchingAction = () => ({
    type: ACTION_MOVIES_FETCHING
})

export const moviesSetAction = payload => ({
    type: ACTION_MOVIES_SET,
    payload
})

export const moviesErrorAction = payload => ({
    type: ACTION_MOVIES_ERROR,
    payload
})




import { applyMiddleware, createStore } from "redux";
import { rootReducers } from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import { loginMiddleware } from "./middleware/loginMiddleware";
import { profileMiddleware } from "./middleware/profileMiddleware";
import { moviesMiddleware } from "./middleware/moviesMiddleware";
import { favouritesMiddleware } from "./middleware/favouritesMiddleware";
import { movieDetailMiddleware } from "./middleware/movieDetailMiddleware";

export const store = createStore(rootReducers, composeWithDevTools(
    applyMiddleware(
        loginMiddleware,
        profileMiddleware,
        moviesMiddleware,
        favouritesMiddleware,
        movieDetailMiddleware
    )
))
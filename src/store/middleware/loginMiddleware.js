import {
    ACTION_LOGIN,
    ACTION_LOGIN_AUTHENTICATE,
    ACTION_LOGIN_SUCCESS,
    loginAuthenticateAction,
    loginErrorAction, loginSuccessAction
} from "../actions/loginActions";
import { authenticateLogin } from "../../components/Login/LoginAPI";
import { profileSetAction } from "../actions/profileActions";

export const loginMiddleware = ({ dispatch }) => next => action => {

    next( action )

    if (action.type === ACTION_LOGIN) {
        dispatch( loginAuthenticateAction(action.payload) )
    }

    if (action.type === ACTION_LOGIN_AUTHENTICATE) {
        authenticateLogin( action.payload )
            .then(userId => {
                dispatch( loginSuccessAction({
                    ...action.payload,
                    id: userId
                }) )
            })
            .catch(error => {
                dispatch( loginErrorAction(error) )
            })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        // Don't add the password to state
        delete action.payload.password
        dispatch( profileSetAction( action.payload ) )
    }

}
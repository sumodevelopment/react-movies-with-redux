import {
    ACTION_PROFILE_CHECK_EXISTING, ACTION_PROFILE_FETCH,
    ACTION_PROFILE_LOGOUT,
    ACTION_PROFILE_SET, profileErrorAction,
    profileSetAction
} from "../actions/profileActions";
import { storageGet, storageRemove, storageSet } from "../../utils/storage"
import { fetchProfile } from "../../components/Profile/ProfileAPI";

const STORAGE_KEY_PROFILE = '_rmr-p'

export const profileMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_PROFILE_CHECK_EXISTING) {
        const profile = storageGet( STORAGE_KEY_PROFILE )
        if (profile !== false) {
            dispatch(profileSetAction( profile ))
        }
    }

    if (action.type === ACTION_PROFILE_FETCH) {
        fetchProfile( action.payload )
            .then(profile => {
                dispatch( profileSetAction( profile ) )
            })
            .catch(error => {
                dispatch( profileErrorAction( error.message ) )
            })
    }

    if (action.type === ACTION_PROFILE_SET) {
        storageSet( STORAGE_KEY_PROFILE, action.payload )
    }

    if (action.type === ACTION_PROFILE_LOGOUT) {
        storageRemove( STORAGE_KEY_PROFILE )
    }

}
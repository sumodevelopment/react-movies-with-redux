import {
    ACTION_MOVIE_DETAIL_FETCH_BY_ID,
    movieDetailErrorAction,
    movieDetailSetByIdAction
} from "../actions/movieDetailActions";

import { fetchMovieById } from "../../components/MovieDetail/MovieDetailAPI";

export const movieDetailMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_MOVIE_DETAIL_FETCH_BY_ID) {

        fetchMovieById( action.payload )
            .then(movie => {
                dispatch( movieDetailSetByIdAction(movie) )
            })
            .catch(error => {
                dispatch( movieDetailErrorAction( error.message ) )
            })

    }
}
import {
    ACTION_FAVOURITES_ADD,
    ACTION_FAVOURITES_FETCH,
    favouritesErrorAction,
    favouritesSetAction
} from "../actions/favouritesActions";
import { fetchFavourites } from "../../components/Favourites/FavouritesAPI";

export const favouritesMiddleware = ({ getState, dispatch }) => next => action => {

    next( action )

    if (action.type === ACTION_FAVOURITES_FETCH) {

        fetchFavourites(action.payload)
            .then(favourites => {
                dispatch( favouritesSetAction( favourites ) )
            })
            .catch(error => {
                dispatch( favouritesErrorAction( error.message ) )
            })
    }

    if (action.type === ACTION_FAVOURITES_ADD) {

    }
}
import { ACTION_MOVIES_FETCHING, moviesErrorAction, moviesSetAction } from "../actions/moviesActions";
import { fetchMovies } from "../../components/Movies/MoviesAPI";

export const moviesMiddleware = ({ getState, dispatch }) => next => action => {

    next( action )

    if ( action.type === ACTION_MOVIES_FETCHING ) {

        const { moviesReducer } = getState()

        if (moviesReducer.movies.length > 0) {
            return dispatch( moviesSetAction( moviesReducer.movies ) )
        }

        fetchMovies().then(movies => {
            dispatch( moviesSetAction( movies ) )
        }).catch(error => {
            dispatch( moviesErrorAction( error.message ) )
        })

    }

}
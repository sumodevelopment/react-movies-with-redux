import {combineReducers} from "redux";
import {loginReducer} from "./loginReducer";
import { profileReducer } from "./profileReducer";
import { moviesReducer } from "./moviesReducer";
import { favouritesReducer } from "./favouritesReducer";
import { movieDetailReducer } from "./movieDetailReducer";

export const rootReducers = combineReducers({
    loginReducer,
    profileReducer,
    moviesReducer,
    favouritesReducer,
    movieDetailReducer
})
import { ACTION_FAVOURITES_ADD, ACTION_FAVOURITES_ERROR, ACTION_FAVOURITES_SET } from "../actions/favouritesActions";

export function favouritesReducer(state = [], action) {
    switch (action.type) {

        case ACTION_FAVOURITES_SET:
            return {
                error: '',
                favourites: action.payload
            }

        case ACTION_FAVOURITES_ADD:
            return {
                error: '',
                favourites: [...state.favourites, action.payload]
            }

        case ACTION_FAVOURITES_ERROR:
            return {
                ...state,
                error: action.payload
            }

        default:
            return state
    }
}
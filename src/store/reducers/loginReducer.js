import {
    ACTION_LOGIN,
    ACTION_LOGIN_ERROR,
    ACTION_LOGIN_SUCCESS
} from "../actions/loginActions";

const initialState = {
    credentials: {
        username: '',
        password: ''
    },
    fetching: false,
    error: ''
}

export function loginReducer(state = initialState, action) {

    switch (action.type) {

        case ACTION_LOGIN:
            return {
                fetching: true,
                credentials: action.payload,
                error: ''
            }

        case ACTION_LOGIN_SUCCESS:
            return {
                fetching: false,
                credentials: {
                    ...state.credentials,
                    password: ''
                },
                error: ''
            }

        case ACTION_LOGIN_ERROR:
            return {
                fetching: false,
                credentials: {
                    username: '',
                    password: ''
                },
                error: action.payload
            }

        default:
            return state
    }

}
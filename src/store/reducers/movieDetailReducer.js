import {
    ACTION_MOVIE_DETAIL_ERROR,
    ACTION_MOVIE_DETAIL_FETCH_BY_ID,
    ACTION_MOVIE_DETAIL_SET_BY_ID
} from "../actions/movieDetailActions";

const initialState = {
    movie: null,
    error: '',
    fetching: false
}

export function movieDetailReducer(state = initialState, action) {
    switch ( action.type ) {

        case ACTION_MOVIE_DETAIL_FETCH_BY_ID:
            return {
                movie: null,
                fetching: true,
                error: ''
            }

        case ACTION_MOVIE_DETAIL_SET_BY_ID:
            return {
                error: '',
                fetching: false,
                movie: action.payload
            }

        case ACTION_MOVIE_DETAIL_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload
            }

        default:
            return state

    }
}
import { ACTION_PROFILE_LOGOUT, ACTION_PROFILE_SET } from "../actions/profileActions";

const initialState = {
    id: null,
    username: '',
    firstName: '',
    lastName: '',
    address: {
        street1: '',
        street2: '',
        country: '',
        postalCode: ''
    },
    favourites: [],
}

export function profileReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_PROFILE_SET:
            return {
                ...state,
                ...action.payload
            }
        case ACTION_PROFILE_LOGOUT:
            return initialState
        default:
            return state
    }
}
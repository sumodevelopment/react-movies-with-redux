import { ACTION_MOVIES_ERROR, ACTION_MOVIES_FETCHING, ACTION_MOVIES_SET } from "../actions/moviesActions";

const initialState = {
    movies: [],
    fetching: false,
    error: ''
}

export function moviesReducer(state = initialState, action) {
    switch( action.type ) {

        case ACTION_MOVIES_FETCHING:
            return {
                ...state,
                error: '',
                fetching: true
            }

        case ACTION_MOVIES_SET:
            return  {
                error: '',
                fetching: false,
                movies: action.payload
            }

        case ACTION_MOVIES_ERROR:
            return  {
                ...state,
                fetching: false,
                error: action.payload
            }

        default:
            return state

    }
}
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
// Routing
import { PublicRoute } from "./hoc/PublicRoute";
import { PrivateRoute } from "./hoc/PrivateRoute";
// State
import { useDispatch } from "react-redux";
import { profileCheckExistingAction } from "./store/actions/profileActions";
// Components
import Login from './components/Login/Login'
import Profile from "./components/Profile/Profile";
import Movies from "./components/Movies/Movies";
import NotFound from "./components/NotFound/NotFound";
import Navbar from "./components/Navbar/Navbar";
import Favourites from "./components/Favourites/Favourites";
import MovieDetail from "./components/MovieDetail/MovieDetail";


function App() {
    // Always initialize App with Checking the session.
    const dispatch = useDispatch()
    dispatch( profileCheckExistingAction() )

    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <Switch>
                    <Route path="/" exact>
                        <Redirect to="/login"/>
                    </Route>
                    <PublicRoute path="/login" component={ Login }/>
                    <PrivateRoute exact path="/movies" component={ Movies } />
                    <PrivateRoute path="/movies/:movieId" component={ MovieDetail } />
                    <PrivateRoute path="/favourites" component={ Favourites } />
                    <PrivateRoute path="/profile" component={ Profile }/>
                    <PublicRoute path="*" component={ NotFound } />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;

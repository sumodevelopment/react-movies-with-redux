import { Redirect, Route } from "react-router-dom";
import { useSelector } from "react-redux";

export const PrivateRoute = props => {

    const { username } = useSelector(state => state.profileReducer)

    if (!username) {
        return <Redirect to="/login" />
    }

    return <Route {...props} />
}
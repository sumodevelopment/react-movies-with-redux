import { API_BASE_URL } from "../../constants/API";

export const fetchProfile = username => {
    return fetch(`${ API_BASE_URL }/users/${username}/profile`)
        .then(r => r.json())
        .then(response => response.data)
}
export const updateProfile = (profile) => {

}
import { useDispatch, useSelector } from "react-redux"
import { useEffect } from "react";
import { profileFetchAction } from "../../store/actions/profileActions";
import Container from "../../hoc/Container";
import Section from "../../hoc/Section";

function Profile() {

    const dispatch = useDispatch()
    const profile = useSelector(state => state.profileReducer)

    useEffect(() => {
        dispatch(profileFetchAction(profile.username))
    }, [ dispatch, profile.username ])

    return (
        <Section>
            <Container>
                <h4>Welcome, { profile.username }</h4>
                { JSON.stringify(profile) }
            </Container>
        </Section>
    )
}

export default Profile
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { favouritesFetchAction } from "../../store/actions/favouritesActions";

function Favourites() {

    const dispatch = useDispatch()
    const { favourites, error } = useSelector(state => state.favouritesReducer)
    const profile = useSelector(state => state.profileReducer)

    useEffect(() => {
        dispatch( favouritesFetchAction( profile.id ) )
    }, [ dispatch, profile ])

    return (
        <div>
            { JSON.stringify(favourites) }
        </div>
    )
}

export default Favourites
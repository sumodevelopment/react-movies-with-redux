import { API_BASE_URL } from "../../constants/API";

export const fetchFavourites = userId => {
    return fetch(`${ API_BASE_URL }/users/${userId}/favourites`)
        .then(r => r.json())
        .then(response => response.data)
}

export const addToFavourites = (userId, movieId) => {
    return fetch(`${ API_BASE_URL }/users/favourites`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ userId, movieId })
    })
        .then(r => r.json())
        .then(response => response.data)
}
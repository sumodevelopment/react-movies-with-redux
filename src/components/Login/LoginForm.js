import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../store/actions/loginActions";

function LoginForm() {

    const dispatch = useDispatch()
    const { fetching } = useSelector(state => state.loginReducer)
    const [ state, setState ] = useState({
        username: '',
        password: ''
    })

    const onInputChange = e => {
        setState({
            ...state,
            [e.target.id]: e.target.value
        })
    }

    const onLoginClick = () => {
        // login clicked
        dispatch(loginAction(state))
    }

    return (
        <form>

            <fieldset>
                <label htmlFor="username">Username</label>
                <input type="text" id="username" onChange={ onInputChange } placeholder="Enter your username"/>
            </fieldset>

            <fieldset>
                <label htmlFor="password">Password</label>
                <input type="password" id="password" onChange={ onInputChange } placeholder="Enter your password"/>
            </fieldset>

            <button type="button" onClick={ onLoginClick }>Login</button>

            { fetching && <p>Attempting login...</p> }
        </form>
    )
}

export default LoginForm
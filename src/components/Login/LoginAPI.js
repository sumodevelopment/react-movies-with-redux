import { API_BASE_URL } from "../../constants/API";

export const authenticateLogin = credentials => {
    return fetch(`${ API_BASE_URL }/users/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            user: {
                ...credentials
            }
        })
    }).then(async response => {
        if (!response.ok) {
            const { error } = await response.json()
            throw Error( error )
        }
        return response.json()
    }).then(response => response.data)
}
import LoginForm from "./LoginForm";
import Section from "../../hoc/Section";
import Container from "../../hoc/Container";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

function Login() {

    const {username} = useSelector(state => state.profileReducer)

    return (
        <>
            { username && <Redirect to="/movies"/> }
            <Section>
                <Container>
                    <h1>Login</h1>
                    <LoginForm/>
                </Container>
            </Section>
        </>
    )
}

export default Login

import Logo from '../../assets/logo.png'
import './Navbar.css'
import { Link, NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import NavbarMenuToggle from "./NavbarMenuToggle";
import { useState } from "react";
import NavbarSignout from "./NavbarSignout";

function Navbar() {

    const [ isActive, setIsActive ] = useState(false)

    const {username} = useSelector(state => state.profileReducer)

    const onActiveToggle = () => setIsActive(!isActive)

    let navbarMenuClass = 'navbar-menu'

    if (isActive) {
        navbarMenuClass += ' is-active'
    }

    return (
        <nav className="navbar sticky-top" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <Link className="navbar-item" to="/movies">
                    <img src={ Logo }
                         alt="Movie catalogue for testing" width=""
                         height="28"/>&nbsp;
                    <b>Movies!</b>
                </Link>
                { username &&
                <NavbarMenuToggle active={ isActive } toggleActive={ onActiveToggle }/>
                }
            </div>

            { username &&
            <div id="navbarBasicExample" className={ navbarMenuClass } onClick={ onActiveToggle }>
                <div className="navbar-end">
                    <NavLink to="/movies" className="navbar-item">
                        <span className="material-icons">movie_creation</span>&nbsp;
                        Movies
                    </NavLink>

                    <NavLink to="/favourites" className="navbar-item">
                        <span className="material-icons">star</span>&nbsp;
                        Favourites
                    </NavLink>

                    <div className="navbar-item has-dropdown is-hoverable">
                            <span className="navbar-link">
                                { username }
                            </span>

                        <div className="navbar-dropdown">
                            <NavLink className="navbar-item" to="/profile">
                                <span className="material-icons">account_circle</span>&nbsp;
                                Profile
                            </NavLink>

                            <hr className="navbar-divider"/>

                            <NavbarSignout/>
                        </div>
                    </div>
                </div>
            </div>
            }
        </nav>
    )
}

export default Navbar
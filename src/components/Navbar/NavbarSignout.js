import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { profileLogoutAction } from "../../store/actions/profileActions";

function NavbarSignout() {

    const dispatch = useDispatch()

    const onSignoutClick = () => {
        if (window.confirm('Are you sure?')) {
            dispatch( profileLogoutAction() )
        }
    }

    return (
        <div className="navbar-item" onClick={ onSignoutClick }>
            <span className="material-icons">lock_open</span>&nbsp;
            Sign out
        </div>
    )
}

export default NavbarSignout
function NavbarMenuToggle({ active, toggleActive }) {

    const onToggleClick = () => toggleActive()

    let navbarToggleClass = 'navbar-burger'

    if (active) {
        navbarToggleClass += ' is-active'
    }

    return (
        <span onClick={ onToggleClick } className={ navbarToggleClass } aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </span>
    )
}

export default NavbarMenuToggle
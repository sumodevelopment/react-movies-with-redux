import { API_BASE_URL } from "../../constants/API";

export const fetchMovieById = movieId => {
    return fetch(`${ API_BASE_URL }/movies/${ movieId }`)
        .then(r => r.json())
        .then(r => r.data)
        .then(movie => {
            if (!movie) {
                throw new Error('Could not find movie with id ' + movieId)
            }
            return movie
        })
}
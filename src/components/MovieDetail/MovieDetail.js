import { Link, useParams } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { movieDetailFetchingByIdAction } from "../../store/actions/movieDetailActions";
import Section from "../../hoc/Section";
import Container from "../../hoc/Container";

function MovieDetail() {

    const dispatch = useDispatch()
    const {movieId} = useParams()
    const {error, movie} = useSelector(state => state.movieDetailReducer)

    useEffect(() => {
        dispatch(movieDetailFetchingByIdAction(movieId));
    }, [])

    return (
        <Section>
            <Container>
                <div>
                    <Link to="/movies">Go back</Link>
                </div>

                { error && <p>{ error }</p> }
                { movie &&
                <>
                    <img src={ movie.cover } alt={ movie.title }/>
                    <p>{ movie.rating }</p>
                    <h2>{ movie.title }</h2>
                    <p>{ movie.description }</p>

                    <h4>Director</h4>
                    <p>{ movie.director }</p>

                    <h4>Cast</h4>
                    <ul>
                        { movie.cast.map(cast => <li key={ cast }>{ cast }</li>)}
                    </ul>

                    <h4>Genre</h4>
                    <ul>
                        { movie.genre.map(genre => <li key={genre}>{ genre }</li>)}
                    </ul>
                </>
                }
            </Container>
        </Section>
    )
}

export default MovieDetail
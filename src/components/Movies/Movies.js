import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { moviesFetchingAction } from "../../store/actions/moviesActions";
import Container from "../../hoc/Container";
import Section from "../../hoc/Section";
import MoviesGrid from "./MoviesGrid";

function Movies() {

    const dispatch = useDispatch()
    const { fetching, error } = useSelector( state => state.moviesReducer )

    useEffect(() => {
        dispatch( moviesFetchingAction() )
    }, [ dispatch ])

    return (
        <Section>
            <Container>
                { error && <p>{ error }</p> }
                { fetching && <p>Getting movies...</p> }
                <MoviesGrid />
            </Container>
        </Section>
    )
}

export default Movies
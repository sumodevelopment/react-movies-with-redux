import { useSelector } from "react-redux";
import styles from './Movies.module.css'
import MovieGridItem from "./MovieGridItem";

function MoviesGrid() {

    const { movies } = useSelector( state => state.moviesReducer )

    return (
        <div className={ styles.MoviesGrid }>
            { movies.map(movie => <MovieGridItem key={ movie.id } movie={ movie } />) }
        </div>
    )
}

export default MoviesGrid
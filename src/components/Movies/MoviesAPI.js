import { API_BASE_URL } from "../../constants/API";

export const fetchMovies = () => {
    return fetch(`${ API_BASE_URL }/movies`)
        .then(response => response.json())
        .then(response => response.data)
}
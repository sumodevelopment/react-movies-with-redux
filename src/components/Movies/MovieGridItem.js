import styles from "./Movies.module.css";
import MovieGridItemRating from "./MovieGridItemRating";
import { useHistory } from "react-router";

function MovieGridItem({movie}) {

    const history = useHistory()

    const onItemClick = () => {
        history.push(`/movies/${ movie.id }`)
    }

    return (
        <div className={ styles.MovieGridItem } onClick={ onItemClick }>
            <img className={ styles.MovieGridItemCover } src={ movie.cover } alt={ movie.title }/>
            <MovieGridItemRating rating={ movie.rating }/>
            <article className={ styles.MovieGridItemMeta }>
                <h4 className={ styles.MovieGridItemTitle }>{ movie.title }</h4>
            </article>

        </div>
    )
}

export default MovieGridItem
import styles from './Movies.module.css'

function MovieGridItemRating({ rating }) {
    return (
        <div className={styles.MovieGridItemRating}>
            <span>{ rating }</span>
            <span className="material-icons">star</span>
        </div>
    )
}

export default MovieGridItemRating